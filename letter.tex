\documentclass[11pt,notitlepage]{report}

\usepackage{soul}
\usepackage{pslatex}
\usepackage{url}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{booktabs}
\usepackage{color,soul}


% Reduce the margins
\addtolength{\oddsidemargin}{-.75in}
\addtolength{\evensidemargin}{-.75in}
\addtolength{\textwidth}{1.5in}

\addtolength{\topmargin}{-1in}
\addtolength{\textheight}{1.5in}
\def\ie{\textit{i.e.},~}
\def\eg{\textit{e.g.},~}
\def\etal{\textit{et al.}~}

\newcommand{\smallnum}[1]{$^{\tiny {\textbf #1}}$}

\title{Response to the Reviewers:\\
Improving the Pull Requests Review Process Using Learning-to-rank Algorithms}
\author{Guoliang Zhao$^{1}$, Daniel Alencar da Costa$^{2}$ and Ying Zou$^{2}$ \\
	$^{1}$School of Computing, Queen's University, Canada;\\ {\tt 17gz2@cs.queensu.ca}\\
	$^{2}$Department of Electrical and Computer Engineering, Queen's University, Canada;\\ {\tt \{daniel.alencar, ying.zou\}@queensu.ca}\\
}
\date{}

\makeatletter
\let\@oddfoot\@empty
\let\@evenfoot\@empty
\makeatother
\pagenumbering{gobble}
\newcommand{\dan}[1]{\textcolor{blue}{{\it [Daniel says: #1]}}}


\begin{document}
\maketitle

\renewcommand{\headrulewidth}{0pt}
\setlength{\parindent}{0in}

Dear Editors and Reviewers of the Empirical Software Engineering (EMSE) Journal
\vspace{5mm}

\indent
The following manuscript is a revised version addressing your recent feedback
about our submission (EMSE-D-18-00160). Below, we include a description of the
changes made to our manuscript in response to each of the reviewers' comments.
We denote quotes using \textbf{bold} and \textit{italic} typeface. Our responses follow below
each reviewer's comment in plain text.\\

With kind regards,\\

Guoliang Zhao \\    
Daniel Alencar da~Costa \\     
Ying Zou \\

\vspace{5mm}

*** Editor Comments ***

\vspace{3mm}

{\bfseries\em E.1 All three reviewers indicate that different reviewers should
be treated differently as they might have personal preferences when it comes to
reviewing the pull requests. This means that a personalised recommendation is
required taking such reviewer attributes into account as skill, motivation and
experience (R2), or previous reviewing history (R3). When doing this more
advanced machine learning techniques might be required (R3).}\\

We agree that this is a valid concern. As the three reviewers explained,
different reviewers on GitHub have different preferences. For example,
different reviewers might be interested in different modules of a project
(R2.2). Though we carefully consider the reviewers' suggestions, the goal of
our work is not to build a personalized model for each reviewer because we do
not intend to replace reviewers' own pull requests (PRs) prioritization
criteria. We aim to help reviewers to make more decisions within a limited time
window. For example, reviewers may use our approach in a limited time session
(e.g., 30 minutes) to take quick decisions before engaging in a more
challenging review. Code reviewers can certainly follow their own
prioritization rules to address these challenging reviews. In our
revised paper, we have clarified our goal in Sections 1 and 2. Another reason
for us to opt for a more generalized approach is that a customized approach for
each reviewer would not work for new reviewers in a software team---the lack of
historical PR reviewing data would not be available.  Therefore, we provide a
generalized approach to help both experienced and new reviewers.\\

Taking the comment R3.1 into consideration, we include the RankNet
learning-to-rank algorithm, which leverages neural networks to model the
underlying ranking function~\cite{burges2005learning}. We show the performance
of the RankNet learning-to-rank model in Sections 4.1 and 4.2.\\

%we noticed that
%integrating the reviewers' preferences in the prediction model would require us
%to have prior knowledge about the code reviewer who would use the model---so
%that our model could adapt to the reviewer's personal preference.  \\
%
% A model per reviewer we don't intend replace their own crit
% can help them in case thy are intested 
%
% even be used by new reviewers
%
% reveal

%We cannot include reviewers related metrics in our approach because we cannot
%identify which reviewer is available to review the new pull request (PR) at the time
%of the prediction. Additionally, as observed by Yu et al~\cite{yu2014should},
%the majority of PRs in GitHub projects are reviewed by a limited number of
%active reviewers (e.g., the most active reviewer in project phantomjs has
%reviewed 77\% of PRs), which means that we cannot obtain enough review history
%from most reviewers. The personalized model would be limited to an limited
%number of reviewers and may not be general enough for other code reviewers or
%new code reviewers. \\
%
%Instead of providing a personalized model for reviewers, the main goal of the
%paper is to provide a generalized approach to predict PRs that are
%likely to receive quick decisions. Our work is not intended to replace the
%reviewers' personal prioritization criteria for selecting PRs, but rather
%to help reviewers to make more decisions within a limited time window. %be another tool at their %hands to improve their working environment. 
%In our revised paper, we have clarified our goal in Sections 1 and 2.\\

Following the suggestion mentioned in R2.2, we provide a new qualitative
analysis in RQ4 (see Section 4.4). We conduct a survey to understand the
perception of code reviewers regarding the usefulness of our approach.  Our
survey consists of 4 open ended questions and 1 Likert scale question.  Our
participants are invited to rate the perceived usefulness of our approach.
87\% of participants agree that our approach can be useful to review PRs. Also,
our participants believe that our approach can complement their existing
practices and help them to merge more PRs in a limited time.  Another perceived
advantage of our approach is that it can avoid the blockage of many PRs due to
the reviewing of a single time-consuming pull request. Given the feedback of
our participants, we believe that our approach indeed can help code reviewers
to reduce the queue of open PRs.\\
  
{\bfseries\em E.2 The ultimate goal of pull request review, i.e., improving quality of
the project. The authors should either explicitly argue why quick
fixes do not affect quality or explicitly take quality into account
(R2).}\\

Thanks for raising this concern. We agree that our approach would not be
useful if it recommended only trivial PRs (e.g., trivial changes to
documentation).  Taking the comment R2.1 into consideration, we randomly
select a statistically significant sample of PRs recommended by our
approach (see Section 5.2).\\

Inspired by the work of Hindle et al~\cite{hindle2008large}, we manually
classify each PR into one of the following categories: bug fixing PR,
enhancement PR, feature add PR and documentation PR (see Table 9). We find that
the majority of PRs are related to enhancement, bug fixing, and new feature
implementation.  Documentation PRs account only for 7\% of the total PRs in our
sample. Our results suggest our approach does not recommend only trivial PRs,
our approach can also recommend PRs (e.g., bug fixing PRs, enhancement PRs and
feature add PRs) to improve the quality of the projects.\\

We have modified our manuscript (Sections 1, 2 and 4.4) to highlight that our
work is not intended to replace the existing reviewers' working practices, but
rather to \texttt{complement} the reviewers' prioritization criteria for
selecting PRs. Code reviewers can certainly follow their own
prioritization rules and focus on PRs with higher priority at first (e.g., PRs
fixing critical bugs or implementing customer requested features). However,
reviewers may use our approach to make several quick decisions before
engaging in more complex reviews that would require more time to complete.\\

{\bfseries\em E.3 the authors should position their approach in the context
of pull request reviews. It is not clear whether ranking quick fixes
high would match the objectives of the project. As indicated by R1
FIFO might be preferred to ensure fairness of the review as opposed to
its efficiency and fairness might be considered more important.
Similarly, R2 wonders whether the proposed ranking technique is
capable not only of outperforming the existing rankings but also meet
the actual needs of the reviewers. To address these concerns the
authors should perform an extensive validation study, including the
controlled experiment suggested by R2 (whether the reviewers prefer
your rankings over the competing approaches) and the observational
study suggested by R1 (whether the approach can benefit the pull
request reviewing process in open source projects).}\\

Thank you for raising these concerns. In fact, we agree with R1
that *only* prioritizing PRs that can be reviewed quickly might
wound the fairness of reviews (i.e., the contributors would not be treated
equally). However, our work does not advocate that a development team should
*only* prioritize PRs that can be quickly reviewed. We modify the Sections 1, 2
and 4.4 to clarify that our approach should not replace the reviewers'
prioritization criteria for selecting PRs. Instead, our approach
should be used to complement the existing PRs prioritization criteria
of reviewers.  For example, reviewers may use our approach in a limited time
session (e.g., 30 minutes) to perform quick decisions before engaging in a more
challenging review.\\    

We also take the comments R1.1.c and R1.1.d into consideration and we add a
paragraph in Section 4.3 to explain how contributors can leverage the important
metrics found in our learning-to-rank model to attract the attention of
reviewers. For example, a new external contributor should seek interactions
with reviewers in the same project to build a social connection with reviewers.
A stronger social interaction between contributors and reviewers increases the
likelihood that a PR will be reviewed. \\

Finally, to address the comment R2.2, we conduct a survey to understand the
perception of reviewers regarding the usefulness of our approach. As described
in E.1, our participants agree with the usefulness of our approach and provide
us with positive feedback. For example, our participants mention that our approach
can help them to prioritize PRs by complementing the other two PRs
prioritization criteria (i.e., FIFO and small size first). From the responses
of our participants, we believe that our approach can benefit the PR
reviewing process in open source projects. \\

\vspace{3mm}

{\bfseries\em *** Reviewer 1 ***}\\

{\bfseries\em R1.1. This paper aims to provide reviewers a recommendation
method, which can help them to find the pull requests that can be
easily solved, and improving the reviewing speed. However, it may not
be that useful.
}\\

We answer each of your concerns below.\\
 
{\bfseries\em R1.1.a This approach can just reduce the time for recognizing
whether a pull request is easily to be handled. While the time can
hardly be quantified. Therefore, it's hard to explain your approach's
effectiveness. }\\

Thank you for raising this concern. The goal of our work is to help reviewers
by recommending pull requests (PRs) that can be reviewed in a short time. For
example, if reviewers wish to spend 30 minutes to take the maximum number of
review decisions within this limited time instead of working on a difficult PR
that can block a large chunk of the reviewer's time, our approach can help them
to focus on quick-to-review PRs at first. We revise Sections 1 and 7 in our
manuscript to clarify this point.\\

The results of the experiments of Section 4.1 and 4.2 suggest that our approach
can help reviewers to take more decisions within a limited time.  Furthermore,
we conduct a survey with GitHub reviewers to evaluate the perceived usefulness
of our approach. We present the survey results in Section 4.4. From the
responses of our survey participants, we are confident that our approach can
help reviewers to merge more PRs in a limited time.\\

Finally, we revise our manuscript to acknowledge that the PR reviewing data
available in GitHub is limited. For example, for a given PR on GitHub, there is
no information indicating when the reviewer started to work on the PR.  We
could not compute the time between the creation of a PR and the time when the
reviewer started to work on the PR. Therefore, it is difficult to quantify the
exact amount of time saved by our approach to spot a quick-to-decide PR. We
revise our manuscript to discuss the limitation on the data in Section 6.\\

{\bfseries\em R1.1.b  Moreover, different reviewers may have different taste,
it's not quite right to treat all reviewers indiscriminately.}\\

Thank you for raising this concern. Indeed, we agree that different reviewers
on GitHub have different preferences. For example, different reviewers may be
interested in different modules of a project (R2.2). As we explain in E.1, the goal of
our work is not to build a personalized model for each reviewer because we do
not intend to replace reviewers' own pull requests (PRs) prioritization
criteria. We aim to help reviewers to make more decisions within a limited time
window. For example, reviewers may use our approach in a limited time session
(e.g., 30 minutes) to take quick decisions before engaging in a more
challenging review. Code reviewers can certainly follow their own
prioritization rules to address these challenging reviews. In our
revised paper, we have clarified our goal in Sections 1 and 2. Another reason
for us to opt for a more generalized approach is that a customized approach for
each reviewer would not work for new reviewers in a software team---the lack of
historical PR reviewing data would not be available.  Therefore, we provide a
generalized approach to help both experienced and new reviewers.\\

{\bfseries\em R1.1.c The reason why reviewers
may use FIFO approach to deal with pull requests is that in this way, all the
contributors are treated equally, which can maintain the enthusiasm of
contributors. However, your recommendation approach is not so friendly to some
outside contributors.}\\

Please see our response to R1.1.d.\\

{\bfseries\em R1.1.d For me, I recommend to improve your approach to remind
contributors to improve their pull requests.}\\

Thank you for both of your comments (i.e., R1.1.c and R1.1.d). As we explain to
E.3, we agree that *only* prioritizing PRs that can be reviewed quickly might
wound the fairness of reviews (i.e., the contributors would not be treated
equally). However, our work does not advocate that a development team should
*only* prioritize PRs that can be quickly reviewed. We modify Sections 1, 2
and 4.4 to clarify that our approach should not replace the reviewers'
prioritization criteria for selecting PRs. Instead, our approach
should be used to complement the existing prioritization criteria of reviewers.
For example, reviewers may use our approach in a limited time session (e.g., 30
minutes) to make quick decisions before engaging in a more challenging
review.\\    

Additionally, we add a paragraph in Section 4.3 to explain how contributors can
leverage the important metrics found in our learning-to-rank model to attract
the attention of reviewers. For example, a new external contributor should seek
interactions with reviewers in the same project to build a social connection
with reviewers.  A stronger social interaction between contributors and
reviewers increases the likelihood that a PR will be reviewed. \\

{\bfseries\em R1.2. In line 23, the threshold is set to 40\% when recognizing
whether a project uses pull based development. How many projects have you used
to get this threshold? Will the performance of your ranking approach improve if
you increase the threshold?}\\

Thanks for raising this valid concern. We use 94 projects to obtain our
threshold of 40\%.  We revise our manuscript (see Section 3) to clarify that in
the projects that the pull based development is not adopted, we find that the
ratio of PRs labeled as \textit{merged} is usually lower than 40\%.\\

We modify our manuscript to add a sensitivity analysis to evaluate the
performance of our approach using different thresholds, i.e., 40\%, 50\%, and
60\% (see Section 5.1).  We find that the differences among performances using
different thresholds are statistically negligible. We observe that the
performance of our approach does not improve when we increase the threshold.
Thus, we conclude that the merge ratio threshold of 0.4 is enough to eliminate
the wrongly labeled PRs. \\

\vspace{3mm}

{\bfseries\em *** Reviewer 2: ***}\\

{\bfseries\em R2.1. The first and most critical concern is the motivation and the proposed
approach. Logically, it seems that reducing the number of
pull-requests in the queue is important, however, as other code
inspection activity (i.e., review) the most paramount object is to
make sure that the quality of the software is maintained.}\\

{\bfseries\em Off course, quality is not included in the experiment design,
however, when you talk about code review, the maintenance of quality
of the project is paramount and is strongly recommended to be
discussed.}\\

{\bfseries\em So I would challenge that 'what is the impact on quality for
quick to fix pull requests'. Just as a trivial example, if we are all
fixing quick fixes of easier "documentation" pull requests, then are
we really getting any work done. What I want to say is that are we
really getting any hard 'work done' with only the quick pull
requests.}\\

{\bfseries\em Of course, reviewers know which PRs are more urgent to be
reviewed.  ** Suggestion 1 ** : authors should prove that quick to fix
does not impact quality. One suggestion is to assess the difficulty or
the quality aspect of the pull request. Maybe in the description, you
can see if it affects quality. Classifying types of the pull requests
such as "documentation" and certain "modules" may prove fruitful. At
least there must be discussions of quality in your ranking.}\\

Thanks for your valuable suggestion. We agree that our approach would not be
useful if it recommended only trivial pull requests (PRs) (e.g., trivial changes to
documentation). To address your comment, we manually analyze a statistical
sample of PRs that are recommended by our approach (305 PRs, see Section 5.2).
We find that the majority of PRs are related to enhancement, bug fixing, and
new feature implementation (accounting for 26\%, 22\% and 14\% of PRs in the
sample). Our results reveal that our approach can recommend PRs that aggregate
value to the projects (i.e., not only trivial PRs).\\

We also modify our manuscript (Sections 1, 2 and 4.4) to highlight that our
work is not intended to replace the existing reviewers' working practices, but
rather to \texttt{complement} the reviewers' prioritization criteria for
selecting PRs. Code reviewers can certainly follow their own
prioritization rules and focus on PRs with higher priority at first (e.g., PRs
fixing critical bugs or implementing customer requested features). However,
reviewers may use our approach to take several quick decisions before
engaging in more complex reviews that would require more time to complete.\\

{\bfseries\em R2.2. The second concern is related to whether the quickest
fixes that are ranked at the top are really meeting the priorities and
objectives of the project (and more directly the reviewers). For
instance, as shown by Gousios et al. [5] developers are concerned with
FIFO and small size. Although your approach is able to outperform
these methods, are they able to meet the needs of the reviewer? For
example, a novice may be interested in the quick to fix, while a more
core reviewer would be only interested in getting fixes that really
affect his/her modules of interest. In my opinion there should be a
mapping from the listing to the kind of reviewer (i.e., novice or
experienced reviewer that is interested in a module). At this stage,
the listings look to generalized for any true usefulness to the
reviewers.}\\

{\bfseries\em **Suggestion 2 **: authors are encouraged to make a match of
the rankings to suit reviewers. I suggest two ways: (a) Maybe you
could match novice reviewers and core reviewers to different lists of
quicker pull requests. Or maybe based on the module of the reviewer. I
assume that novice reviewers would like to review general but quicker
fixes while core contributors are more interested in the quicker but
more specialized fixes. This would make you ranking more valuable I
believe.  (b) Another option would be to make a survey for reviewers
to fully understand how they would react to your ranking. For
instance, in [5] they cite FIFO and small size, yet you could make an
experiment and see if they pick your rankings over the other two. In
[5], they performed a user study to evaluate their recommended lists
of prioritized pull requests.}\\

Thank you for your useful suggestions, we provide a new qualitative
analysis in RQ4 (see Section 4.4). We conduct a survey to understand the
perception of code reviewers regarding the usefulness of our approach.  Our
survey consists of 4 open ended questions and 1 Likert scale question.  Our
participants are invited to rate the perceived usefulness of our approach.
87\% of participants agree that our approach can be useful to review pull
requests (PRs). Also, our participants believe that our approach can complement
their existing practices and help them to merge more PRs in a limited time.
Another perceived advantage of our approach is that it can avoid the blockage
of many PRs due to the reviewing of a single time-consuming pull
request. Given the feedback of our participants, we believe that our approach
indeed can help code reviewers to reduce the queue of open PRs.\\

%\dan{please synchronize the text from the comment E1.}
%
%Thank you for your useful suggestions. We follow your
%suggestion and perform a new qualitative analysis in RQ4 (see Section 4.4). We
%conduct a survey to understand what is the perception of code reviewers
%regarding the usefulness of our approach. Our survey consists of 4 open
%ended questions and 1 Likert scale question. Our participants are invited to
%rate the perceived usefulness of our approach. 87\% of participants agree that
%our approach can improve their efficiency in reviewing pull requests (PRs). Our
%participants believe that our approach can help them to merge more PRs in a
%limited time. Our participants mention that our approach can avoid the blockage
%of many pull requests due to the reviewing of a single time-consuming pull
%request. Our participants also mention that our approach can complement their
%existing working environment by helping them to prioritize potentially quick
%reviews. Given the feedback of our participants, we believe that our approach
%can help code reviewers to reduce the queue of open pull requests.\\

\vspace{3mm}

{\bfseries\em *** Reviewer 3: ***}\\

{\bfseries\em  R3.1.  There is a little novelty and interesting findings in
this work.  Both 18 metrics and 6 learning-to-rank algorithms are not
novel, and were proposed several years ago, and usually used by many
related works.}\\

Thank you for raising your concern. Although many metrics in our approach have
been studied in previous research, we manage to integrate all metrics together
by using learning-to-rank algorithms. We also include metrics that have not
been studied in the context of pull requests (PRs), such as code complexity and
readability metrics. The goal of our work is original. We aim to help
reviewers to prioritize PRs by recommending PRs that can be
reviewed in a limited time. For example, if reviewers wish to spend 30
minutes to take the maximum number of review decisions, our approach can help
them to focus on PRs that can be reviewed quickly at first. We revise Sections 1
and 7 to clarify this point. Based on our experiment results
(see Section 4.1 and 4.2) and the responses that we received from our survey (see
Section 4.4), we are confident that our approach can help reviewers to
prioritize PRs and to merge more PRs in a limited time.\\

{\bfseries\em Suggestions:}\\
{\bfseries\em ** Authors should apply Neural Networks and deep learning
algorithms to do empirical studies.}\\

Thank you for your suggestion. We take your suggestion into consideration and
apply the RankNet learning-to-rank algorithm, which leverages neural networks
to model the underlying ranking function~\cite{burges2005learning}. We do not
apply deep learning models because the size of our dataset does not have the
necessary scale, so that a deep learning model could bring the intended
benefits.\\

{\bfseries\em ** Authors can consider trying to build the reviewer profiles
from their historical behavior logs, and thus can recommend PRs more
precisely.}\\

Thank you for raising this concern. Indeed, we agree that different reviewers
on GitHub have different preferences. For example, different reviewers may be
interested in different modules of a project (R2.2). As we explain in E.1, the goal of
our work is not to build a personalized model for each reviewer because we do
not intend to replace reviewers' own pull requests (PRs) prioritization
criteria. We aim to help reviewers to make more decisions within a limited time
window. For example, reviewers may use our approach in a limited time session
(e.g., 30 minutes) to take quick decisions before engaging in a more
challenging review. Code reviewers can certainly follow their own
prioritization rules to address these challenging reviews. In our
revised paper, we have clarified our goal in Sections 1 and 2. Another reason
for us to opt for a more generalized approach is that a customized approach for
each reviewer would not work for new reviewers in a software team---the lack of
historical PR reviewing data would not be available.  Therefore, we provide a
generalized approach to help both experienced and new reviewers.\\

{\bfseries\em R3.2. To my knowledge, the challenges of this work are
relatively small.  The algorithms are not difficult to implement, and
also there are some open sources about these algorithms. Are these
metrics difficult to calculate on 74 Java projects?}\\

{\bfseries\em Suggestions:}\\

{\bfseries\em  ** If there are some big challenges, please depict them
explicitly in the Paper.}\\

Thanks for discussing this matter. Despite not using a very hard methodology to
implement our work, we believe that our work indeed addresses a big challenge.
We improve Section 1 to highlight that the load of PRs tends to
increase while the number of reviewers and the decisions taken tend to remain
constant. This is a big problem because the success of open source projects
depends on addressing the concerns of users and external contributors.  By
proposing an approach that can help reviewers to review and give expedite
feedback on more PRs, our research is useful for the community. In fact, by
considering the feedback from our participants (see our new qualitative study
in Section 4.4), we observe that our approach can indeed help code reviewers to
give more expedite feedback (see also comments R1.1.a and R2.2).  We take your
comment into consideration and we revise the Introduction (Section 1) to better
highlight the challenges that our paper addresses.\\

\bibliographystyle{IEEEtran}
\bibliography{reference}
\end{document}
